module gitlab.com/beabys/ayotl

go 1.20.0

require (
	github.com/mitchellh/mapstructure v1.5.0
	github.com/spf13/cast v1.6.0
	github.com/stretchr/testify v1.9.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
